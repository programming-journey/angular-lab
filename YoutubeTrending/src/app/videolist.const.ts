
export const videoList = [
  {
    'title': 'Missy Elliott - Throw It Back [Official Music Video]',
    'publishedAt': '2019-08-23T04:00:10.000Z',
    'channelTitle': 'Missy Elliott'
  },
  {
    'title': 'Taylor Swift - Lover',
    'publishedAt': '2019-08-22T21:30:09.000Z',
    'channelTitle': 'TaylorSwiftVEVO'
  },
  {
    'title': 'Tom MacDonald  \'Im Sorry\'',
    'publishedAt': '2019-08-23T17:55:30.000Z',
    'channelTitle': 'Tom MacDonald'
  },
  {
    'title': 'Lil Tjay - F.N (Official Video)',
    'channelTitle': 'liltjayVEVO',
    'publishedAt': '2019-08-22T20:00:11.000Z',
  },
  {
    'title': 'First Day Back To School *NEW POPULAR GIRL*',
    'publishedAt': '2019-08-23T18:00:51.000Z',
    'channelTitle': 'Dan and Riya'
  },
  {
    'title': 'Surviving 24 Hours On A Deserted Island',
    'publishedAt': '2019-08-22T20:00:07.000Z',
    'channelTitle': 'MrBeast'
  },
  {
    'title': 'NLE Choppa Runs Into Lil Baby While Shopping For Jewelry!',
    'publishedAt': '2019-08-22T19:00:11.000Z',
    'channelTitle': 'Icebox',
  },
  {
    'title': 'My Type (feat. City Girls & Jhené Aiko) [Remix] [Official Audio]',
    'publishedAt': '2019-08-23T04:11:23.000Z',
    'channelTitle': 'Official Saweetie'
  },
  {
    'title': 'Best Of DJ D-Wrek vs. Wild ‘N Out Cast (Vol. 1) 😂 | MTV',
    'publishedAt': '2019-08-23T16:00:05.000Z',
    'channelTitle': 'Wild \'N Out'
  }
];
