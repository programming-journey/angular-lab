import { Component } from '@angular/core';
import {videoList} from './videolist.const';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'TrendingVideoList';
  list = videoList;
}
